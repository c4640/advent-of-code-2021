with open("depths.txt", "r") as file:
  lines = file.readlines()
  depths = [int(i.strip()) for i in lines]

depth_increase_count = 0
for i in range(len(depths)):
  if i == 0:
    continue

  if depths[i] > depths[i - 1]:
    depth_increase_count += 1

print(f"""
Total depth measurements: {len(depths)}
Total depth increases: {depth_increase_count}
""")