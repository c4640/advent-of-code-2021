with open("depths.txt", "r") as file:
  depths = [int(i.strip()) for i in file.readlines()]

window_size = 3
windows = [tuple(depths[i:(i + window_size)]) for i in range(len(depths)) if i + window_size <= len(depths)]

depth_increase_count = 0
for i in range(len(windows)):
  if i == 0:
    continue

  if sum(windows[i]) > sum(windows[i - 1]):
    depth_increase_count += 1

print(f"""
Total depth windows: {len(windows)}
Total depth increases: {depth_increase_count}
""")