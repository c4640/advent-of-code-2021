with open("diagnostic.txt", "r") as file:
  data = [i.strip() for i in file.readlines()]

#data = ["00100", "11110", "10110", "10111", "10101", "01111", "00111", "11100", "10000", "11001", "00010", "01010"]

gamma_rate_bits = ""
epsilon_rate_bits = ""
for i in range(len(data[0])):
  bits = [s[i] for s in data]
  bit_count = {
    0: bits.count("0"),
    1: bits.count("1")
  }

  most_significant_bit = 0 if bit_count.get(0) > bit_count.get(1) else 1
  least_significant_bit = 0 if bit_count.get(0) < bit_count.get(1) else 1
  gamma_rate_bits += str(most_significant_bit)
  epsilon_rate_bits += str(least_significant_bit)

gamma_rate = int(gamma_rate_bits, base=2)
epsilon_rate = int(epsilon_rate_bits, base=2)

print(f"""
Gamma rate: {gamma_rate} (binary: {gamma_rate_bits})
Epsilon rate: {epsilon_rate} (binary: {epsilon_rate_bits})
Power consumption: {gamma_rate * epsilon_rate}
""")