with open("diagnostic.txt", "r") as file:
  data = [i.strip() for i in file.readlines()]

#ata = ["00100", "11110", "10110", "10111", "10101", "01111", "00111", "11100", "10000", "11001", "00010", "01010"]

oxygen_candidates =  list(data)
oxygen_rating_intermediate = ""
oxygen_rating = ""
co2_candidates = list(data)
co2_rating_intermediate = ""
co2_rating = ""

for i in range(len(data[0])):
  if len(oxygen_candidates) <= 1 and len(co2_candidates) <= 1:
    break

  if len(oxygen_candidates) > 1:
    bits_oxygen = [s[i] for s in oxygen_candidates]
    bit_count_oxygen = {
      bits_oxygen.count("0"): 0,
      bits_oxygen.count("1"): 1
    }

    most_significant_bit = 1 if len(bit_count_oxygen.keys()) == 1 else bit_count_oxygen.get(max(bit_count_oxygen.keys()))
    oxygen_rating += str(most_significant_bit)

    candidates = list(filter(lambda x: x.startswith(oxygen_rating), oxygen_candidates))    
    oxygen_candidates = candidates
    
    if len(oxygen_candidates) == 1:
      oxygen_rating = oxygen_candidates[0]
      print(f"Oxygen: {oxygen_rating} in iteration {i}")
  
  if len(co2_candidates) > 1:
    bits_co2 = [s[i] for s in co2_candidates]
    bit_count_co2 = {
      bits_co2.count("0"): 0,
      bits_co2.count("1"): 1
    }

    least_significant_bit = 0 if len(bit_count_co2.keys()) == 1 else bit_count_co2.get(min(bit_count_co2.keys()))
    co2_rating += str(least_significant_bit)

    candidates = list(filter(lambda x: x.startswith(co2_rating), co2_candidates))
    co2_candidates = candidates

    if len(co2_candidates) == 1:
      co2_rating = co2_candidates[0]
      print(f"CO2: {co2_rating} in iteration {i}")

print(oxygen_rating, co2_rating)
oxygen_rating_dec = int(oxygen_rating, base=2)
co2_rating_dec = int(co2_rating, base=2)

print(f"""
Oxygen rating: {oxygen_rating_dec} (binary: {oxygen_rating})
CO2 rating: {co2_rating_dec} (binary: {co2_rating})
Life support rating: {oxygen_rating_dec * co2_rating_dec}
""")