from dataclasses import dataclass
from typing import List


class BingoBoard:
    def __init__(self, board_list: List[List[int]]) -> None:
        self.board_matrix = board_list
        self.marker_matrix = []
        self._initialize_marker_matrix()

    @dataclass
    class BoardCoordinate:
        x: int
        y: int


    def check_number(self, number: int) -> bool:
        """
        Check if board contains a number and then check if it board has bingo

        Args:
            number (int): picked number

        Returns:
            bool: returns True if board has bingo, otherwise returns False
        """
        if self.check_for_bingo():
            return True

        c = self.BoardCoordinate(0, 0)

        for row in self.board_matrix:
            if number in row:
                c.x = row.index(number)
                self.marker_matrix[c.y][c.x] = True

            c.y += 1

        return self.check_for_bingo()
    
    def check_for_bingo(self) -> bool:
        if any([all(row) for row in self.marker_matrix]):
            return True

        for i in range(len(self.marker_matrix[0])):
            if all([row[i] for row in self.marker_matrix]):
                return True
        
        return False
    
    def sum_unmarked_numbers(self) -> int:
        nums = []

        c = self.BoardCoordinate(0,0)
        for row in self.marker_matrix:
            c.x = 0
            for x in row:
                if not x:
                    nums.append(self.board_matrix[c.y][c.x])
                
                c.x += 1
                
            c.y += 1
        
        return(sum(nums))

    
    def _initialize_marker_matrix(self):
        mm = [[False for y in x] for x in self.board_matrix]
        self.marker_matrix = mm


def parse_boards(boards_raw_data: List[str]) -> List[BingoBoard]:
    boards = []

    idx = 0
    new_board_list = []
    for line in boards_raw_data:
        if len(line) > 0:
            new_board_list.append([int(i) for i in line.split()])

        if len(line) == 0 or idx == len(boards_raw_data) - 1:
            if len(new_board_list) > 0:
                boards.append(BingoBoard(new_board_list))
                new_board_list = []
    
            idx += 1
            continue

        idx += 1
    
    return boards

with open("input.txt", "r") as file:
  raw_data = [i.strip() for i in file.readlines()]


numbers = [int(n) for n in raw_data[0].split(",")]

boards = parse_boards(raw_data[2:])
print(boards)

bingo_boards = []
last_number = None
for num in numbers:
    for board in boards:
        has_bingo = board.check_number(num)
        if has_bingo and board not in bingo_boards:
            bingo_boards.append(board)
            last_number = num

print(f"Board result: {bingo_boards[-1].sum_unmarked_numbers() * last_number}")