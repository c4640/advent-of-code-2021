from dataclasses import dataclass
from typing import List

@dataclass
class Point:
  x: int
  y: int


@dataclass
class Line:
  p0: Point
  p1: Point

  def get_line_points(self) -> List[Point]:
    if self.p0.x == self.p1.x:
      if self.p0.y < self.p1.y:
        return [Point(self.p0.x, y) for y in range(self.p0.y, self.p1.y + 1)]
      else:
        return [Point(self.p0.x, y) for y in range(self.p1.y, self.p0.y + 1)]

    if self.p0.y == self.p1.y:
      if self.p0.x < self.p1.x:
        return [Point(x, self.p0.y) for x in range(self.p0.x, self.p1.x + 1)]
      else:
        return [Point(x, self.p0.y) for x in range(self.p1.x, self.p0.x + 1)]
  
  def get_max_x_y(self):
    return max([self.p0.x, self.p1.x]), max([self.p0.y, self.p1.y])


class Grid:
  def __init__(self, width, height) -> None:
      self.point_matrix = [["." for _ in range(width)] for _ in range(height)]
  
  def __str__(self) -> str:
      s = "\n".join(["".join([str(p) for p in row]) for row in self.point_matrix])

      return s

  def mark_point(self, point: Point):
    if self.point_matrix[point.y][point.x] == ".":
      self.point_matrix[point.y][point.x] = 1
    else:
      self.point_matrix[point.y][point.x] += 1
  
  def count_points_above_threshold_inclusive(self, threshold: int) -> int:
    count = 0

    for row in self.point_matrix:
      count += len(list(filter(lambda x: x >= threshold, [int(p) for p in row if isinstance(p, int)])))
    
    return count


with open("input.txt", "r") as file:
  raw_data = [i.strip() for i in file.readlines()]


lines: List[Line] = []

for line in raw_data:
  p0, p1 = line.split(" -> ")
  lines.append(Line(Point(int(p0.split(",")[0]), int(p0.split(",")[1])), Point(int(p1.split(",")[0]), int(p1.split(",")[1]))))

candidate_lines: List[Line] = []

for line in lines:
  if line.p0.x == line.p1.x or line.p0.y == line.p1.y:
    candidate_lines.append(line)

max_x = max([p[0] for p in [l.get_max_x_y() for l in candidate_lines]])
max_y = max([p[1] for p in [l.get_max_x_y() for l in candidate_lines]])

grid = Grid(max_x + 1, max_y + 1)

for line in candidate_lines:
  for p in line.get_line_points():
    grid.mark_point(p)

print(grid.count_points_above_threshold_inclusive(2))