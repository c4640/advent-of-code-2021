from dataclasses import dataclass

with open("plan.txt", "r") as file:
  plan_raw = [i.strip() for i in file.readlines()]

@dataclass
class Instruction:
  direction: str
  units: int

@dataclass
class Coordinates:
  aim: int
  forward: int
  depth: int

  def update(self, instruction: Instruction) -> None:
    if instruction.direction == 'forward':
      self.forward += instruction.units
      self.depth += instruction.units * self.aim
    
    if instruction.direction == 'up':
      self.aim -= instruction.units
    
    if instruction.direction == 'down':
      self.aim += instruction.units
    
    self.depth = max(0, self.depth)

plan = list(map(lambda x: Instruction(direction=x[0], units=int(x[1])), [l.split(" ") for l in plan_raw]))

coordinates = Coordinates(aim=0, forward=0, depth=0)

for instr in plan:
  coordinates.update(instr)

print(f"""
Final x: {coordinates.forward}  
Final y: {coordinates.depth}

Product: {coordinates.forward * coordinates.depth}
""")